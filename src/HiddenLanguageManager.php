<?php

namespace Drupal\simple_sitemap_hidden_language;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Class HiddenLanguageManager with some helper functions.
 *
 * @package Drupal\simple_sitemap_hidden_language
 */
class HiddenLanguageManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The language manager interface.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * Default site language.
   * 
   * @var string
   */
  private $defaultLanguage;

  /**
   * HiddenLanguageManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    LanguageManagerInterface $languageManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;

    $languageId = $this->languageManager->getDefaultLanguage()->getId();
    $this->defaultLanguage = $languageId;
  }

  /**
   * Resolve drupal internal route name by entity type and id.
   *
   * @param string $entityType
   *   Entity type used to load an entity object.
   * @param string $id
   *   Entity id used to load an entity object.
   *
   * @return false|string
   *   The route name or false.
   */
  public function getRouteNameByEntityTypeAndId(string $entityType, string $id) {
    try {
      $entity = $this->entityTypeManager->getStorage($entityType)->load($id);
      return $entity->toUrl()->getRouteName();
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Verify by lang-code if the language is hidden and if it's the language.
   *
   * @param string $langcode
   *   A string with the lang-code.
   *
   * @return mixed
   *   Returns an array with containing the hidden and default information.
   */
  public function isLanguageHidden(string $langcode) {
    $result = &drupal_static(__FUNCTION__);

    if (isset($result[$langcode])) {
      return $result[$langcode];
    }

    // Configurable language storage.
    $configurableLanguage = $this->entityTypeManager->getStorage('configurable_language');
    $language = $configurableLanguage->load($langcode);
    $result[$langcode]['hidden'] = $language->getThirdPartySetting('hidden_language', 'hidden', FALSE) ?? FALSE;
    $result[$langcode]['default'] = $this->defaultLanguage == $langcode;
    return $result[$langcode];
  }

}
